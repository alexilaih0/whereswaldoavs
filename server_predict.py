import json
import socketserver
import os
import time
import argparse

from libs.image_conversion import base64_to_image, image_to_base64
from libs.SocketClient import client
from libs.helper import timestamp, toJson
from predict import *
from libs.tiramisu import *
from libs.preprocessing import load_image
from multiprocessing import Pool

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
add_arg = parser.add_argument

add_arg('imgs', nargs='*', default=[])
add_arg('--ip', type=str)
add_arg('--i', default=0, type=int)
add_arg('--model', default='./model/model2_10200epochs.h5', type=str)
add_arg('--output_path', default='./outputs/', type=str)
add_arg('--img_size', default=(396, 495), type=tuple, help='resolution to load images')
args = parser.parse_args()

if not os.path.exists('outputs'):
    os.makedirs('outputs')

if not os.path.exists('tmp'):
    os.makedirs('tmp')


class MyTCPHandler(socketserver.StreamRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls
        self.data = self.rfile.readline().strip()

        print(timestamp("Image received {}".format(self.client_address[0])))
        # print(self.data.decode())

        ip = self.client_address[0]
        a = json.loads(self.data.decode())
        # print("Index : " + a['index'])
        # print("Base64: " + a['base64'][1:])

        # Likewise, self.wfile is a file-like object used to write back
        # to the client
        base64_to_image(a['base64'][1:], "./tmp/tmp" + a['index'] + ".jpg")
        self.wfile.write("1".encode("utf-8"))
        pool.apply_async(func=work, args=(a['index'], ip))


def work(index, ip):
    print(timestamp("Worker Started index: " + str(index)))
    start = time.time()
    import keras
    input_shape = (224, 224, 3)

    img_input = Input(shape=input_shape)
    x = create_tiramisu(2, img_input, nb_layers_per_block=[4, 5, 7, 10, 12, 15], p=0.2, wd=1e-4)
    model = Model(img_input, x)

    model.compile(loss='categorical_crossentropy',
                  optimizer=keras.optimizers.RMSprop(1e-3),
                  metrics=["accuracy"],
                  sample_weight_mode='temporal')

    model.load_weights(args.model)

    start2 = time.time()
    full_img = load_image("./tmp/tmp" + index + ".jpg")
    full_img_r, full_pred = waldo_predict(model, full_img)
    mask = prediction_mask(full_img_r, full_pred)
    end = time.time()

    fulltime = end-start
    predicttime = end-start2
    deltatime = fulltime-predicttime

    print(timestamp(
        'Execution Time image index ' + str(index) + ': Fulltime: ' + str(fulltime) + ' - Predicttime: ' + str(
            predicttime) + ' - Delta: ' + str(deltatime)))
    mask.save(os.path.join(args.output_path, 'output_' + str(index) + '.png'))
    finished(index, ip)


def finished(index, ip):
    print(timestamp("Worker ") + str(index) + " finished")
    img_path = './outputs/output_' + str(index) + '.png'
    imnp = load_image(img_path)
    im = Image.fromarray(imnp)
    string = image_to_base64(im, "PNG")
    jsonwrapper = toJson(index, string)
    print(timestamp("Sending result " + str(index) + " to: " + str(ip)))
    client(ip, 9998, jsonwrapper)


if __name__ == "__main__":
    HOST, PORT = args.ip, 9999
    pool = Pool()

    # Create the server, binding to localhost on port 9999
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    print(timestamp('Server Ready.'))
    server.serve_forever()
