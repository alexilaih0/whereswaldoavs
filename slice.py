import argparse
import json
import os
import ntpath
import numpy as np
import time
import math
from PIL import Image
from libs.SocketClient import client
from libs.preprocessing import load_image
from libs.helper import toJson, loadComputersConfig, timestamp
from libs.image_conversion import image_to_base64, base64_to_image
from server_master import server_master

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
add_arg = parser.add_argument

add_arg('--img')
add_arg('--slice', default='./sliceJson/slice1.json')
add_arg('--output_path', default='./slices/', type=str)
add_arg('--ip', type=str)
args = parser.parse_args()


def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def reconstructImage(num):
    imgArr = []
    jsonInvert = args.slice[:-5] + '-invert.json'
    g = open(jsonInvert)
    slicer2 = json.load(g)

    for j, slic2 in enumerate(slicer2["slices"]):
        path = "./tmp/predict-uninverted-" + str(j) + ".jpg"
        full_img2 = load_image(path)
        h, w, z = full_img2.shape
        imgArr.append(
            full_img2[int(eval(slic2['x1'])):int(eval(slic2['x2'])),
                      int(eval(slic2['y1'])):int(eval(slic2['y2']))])

    for j in range(num):
        im3 = Image.fromarray(imgArr[j])
        im3.save('./tmp/inverted-' + str(j) + '.png')

    rec_image = imgArr[0]

    for k, image in enumerate(imgArr):
        if k > 0:
            rec_image = np.hstack((rec_image, image))

    im2 = Image.fromarray(rec_image)
    im2.save('./tmp/result.png')


start = time.time()

if not os.path.exists('slices'):
    os.makedirs('slices')

f = open(args.slice)
slicer = json.load(f)
f.close()
ntpath.basename("a/b/c")

base64strings = []

full_img = load_image(args.img)
h, w, z = full_img.shape
panels = []

for x, slic in enumerate(slicer["slices"]):
    panels.append(
        full_img[int(eval(slic['x1'])):int(eval(slic['x2'])), int(eval(slic['y1'])):int(eval(slic['y2']))])
    im = Image.fromarray(panels[x])
    base64strings.append(image_to_base64(im))

computers = loadComputersConfig()
numberComps = len(computers)
print(str(len(base64strings)))

for i, s in enumerate(base64strings):
    jsonWrapper = toJson(i, s)
    ip = computers[i % numberComps]['ip']
    print(timestamp("Sending to: " + ip))
    client(ip, 9999, jsonWrapper)

server_master(args.ip, len(base64strings))
reconstructImage(len(base64strings))
print(timestamp("Total execution time: " + str(time.time() - start) + " seconds"))
