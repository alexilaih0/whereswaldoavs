import json
import socketserver

from libs.helper import timestamp
from libs.image_conversion import base64_to_image

from threading import Thread
from time import sleep


class MyTCPHandler(socketserver.StreamRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        global numOfReceived, numOfImages
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls
        self.data = self.rfile.readline().strip()

        print(timestamp("Image received {}".format(self.client_address[0])))
        # print(self.data.decode())

        a = json.loads(self.data.decode())

        # Likewise, self.wfile is a file-like object used to write back
        # to the client
        base64_to_image(a['base64'][1:], "./tmp/predict-uninverted-" + a['index'] + ".jpg")
        self.wfile.write("1".encode("utf-8"))
        numOfReceived += 1


numOfReceived = 0
numOfImages = 0


def checkStopCondition(arg):
    global numOfImages, numOfReceived
    while True:
        if numOfImages == numOfReceived:
            print(timestamp('Received all images, trying to shutdown ...'))
            arg.shutdown()
            exit(1)
        sleep(1)


def server_master(ip, num):
    global numOfReceived, numOfImages
    numOfReceivedImages = 0
    numOfImages = num

    HOST, PORT = ip, 9998

    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

    print(timestamp('Starting watcher threads.'))
    thread = Thread(target=checkStopCondition, args=(server,))

    thread.daemon = False
    thread.start()

    print(timestamp('Master Server Ready. Waiting for ' + str(numOfImages) + ' images.'))
    server.serve_forever()

    thread.join()
    print(timestamp("Server stopped."))

    server.server_close()
    server.shutdown()
