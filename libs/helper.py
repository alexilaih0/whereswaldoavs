import json
import time

def toJson(i, string):
    base = "{{ \"index\": \"{}\", \"base64\": \"{}\" }}".format(i, string)
    return base


def loadComputersConfig():
    with open("./computers.json", "r") as fh:
        s = fh.read()
        return json.loads(s)["computers"]

def timestamp(string):
    return "["+str(time.strftime('%H:%M:%S', time.gmtime(time.time()+3600)))+"] " +string