import argparse
import base64
from io import BytesIO


def image_to_base64(img, f="JPEG"):
    buffered = BytesIO()
    img.save(buffered, format=f)
    str2 = base64.b64encode(buffered.getvalue())
    return str2


def base64_to_image(base64string, filename):
    with open(filename, "wb") as fh:
        fh.write(base64.decodebytes(str.encode(base64string)))
