import socket
import sys
from libs.helper import timestamp

HOST = sys.argv[1]
PORT = int(sys.argv[2])
data = " ".join(sys.argv[3:])

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    sock.sendall(bytes(data + "\n", "utf-8"))

    # Receive data from the server and shut down
    received = str(sock.recv(1024), "utf-8")
finally:
    sock.close()

#print("Sent:     {}".format(data))
print(timestamp("Received: {}".format(received)))
